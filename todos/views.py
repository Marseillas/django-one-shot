from django.shortcuts import render
from .models import Todolist

def todos_list(request):
    list = Todolist.objects.all()
    context = {
        "List": list
    }
    return render(request, "todos/todo_list.html", context)
