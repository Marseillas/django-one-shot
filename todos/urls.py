from django.urls import path
from .views import todos_list

urlpatterns = [
    path("todos", todos_list, name="To_do_List")
]
