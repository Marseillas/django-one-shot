from django.db import models

class Todolist(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField()

    def __str__(self) -> str:
        return self.name
